

resource "aws_s3_bucket" "website" {
  bucket = "${var.S3}"
  region = "${var.region}"
  acl    = "public-read"

  website {
    index_document = "index.html"
    error_document = "error.html"
  }
  force_destroy = true
}
resource "aws_s3_bucket_object" "index" {
  bucket = "${var.S3}"
  acl    = "public-read"
  key    = "index.html"
  source = "C:/terraform_dir/index.html"
  content_type = "text/html"
}
resource "aws_s3_bucket_object" "Success" {
  bucket = "${var.S3}"
  acl    = "public-read"
  key    = "Success.jpg"
  source = "C:/terraform_dir/Success.jpg"
}