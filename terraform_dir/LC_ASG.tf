

resource "aws_launch_configuration" "as_conf" {
  name   = "terraform-lc"
  image_id      = "${var.ami_map["${var.region}"]}"
  instance_type = "t2.micro"
  key_name      = "${var.ssh_key_name}"
  user_data = "${file("user_data.sh")}"
  depends_on = ["aws_security_group.ec2_instance_sg"]
  security_groups = [ "$aws_security_group.ec2_instance_sg.id" ]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "ASG" {
  name_prefix          = "terraform-asg-example"
  launch_configuration = "${aws_launch_configuration.as_conf.name}"
  availability_zones   = [ "${data.aws_availability_zones.env_available_azs.names[var.availability_zones[1]]}" ]
  min_size             = 2
  max_size             = 6
  load_balancers       = ["${var.ELB}"]

  lifecycle {
    create_before_destroy = true
  }
}

# Create auto scaling policy for auto scaling group us-east-2a
# Uses target tracking to target 75% CPU load
resource "aws_autoscaling_policy" "CPUpolicy" {
  name			= "web-server-auto-scaling-policy"
  policy_type		= "TargetTrackingScaling"
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
    target_value	= 75.0
    }
  autoscaling_group_name = "${aws_autoscaling_group.ASG.name}"
}