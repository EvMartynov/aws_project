variable "ami_map" {
  type = "map"
  default = {
    "us-east-1" = "ami-6356761c"
    "us-east-2" = "ami-a2ecd4c7"
  }
}

variable "S3" {
  type = "string"
  default = "www.marttest.com"
}

variable "ELB" {
  type = "string"
  default = "test-env-ELB"
}

variable "region" {
  type = "string"
  default = "us-east-2"
}

variable "availability_zones" {
  type = "list"
  default = ["0", "1"]
}

variable "environment_name" {
  type = "string"
  default = "test-env_mart"
}

variable "ssh_key_name" {
  type = "string"
  default = "martynov-test"
}
