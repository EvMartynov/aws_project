output "vpc_id" {
  value = "${aws_vpc.env_vpc.id}"
}

output "vpc_cidr" {
  value = "${aws_vpc.env_vpc.cidr_block}"
}

output "lc_name" {
  value = "${aws_launch_configuration.as_conf.name}"
}

output "asg_name" {
  value = "${aws_autoscaling_group.ASG.name}"
}

output "pub_subnet_a_id" {
  value = "${aws_subnet.pub_subnet_a.id}"
}

output "pub_subnet_b_id" {
  value = "${aws_subnet.pub_subnet_b.id}"
}

output "priv_subnet_a_id" {
  value = "${aws_subnet.priv_subnet_a.id}"
}

output "priv_subnet_b_id" {
  value = "${aws_subnet.priv_subnet_b.id}"
}

output "availability_zone_a_name" {
  value = "${data.aws_availability_zones.env_available_azs.names[var.availability_zones[0]]}"
}

output "availability_zone_b_name" {
  value = "${data.aws_availability_zones.env_available_azs.names[var.availability_zones[1]]}"
}

output "region_name" {
  value = "${var.region}"
}
