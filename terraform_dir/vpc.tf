resource "aws_vpc" "env_vpc" {
  cidr_block         = "172.31.0.0/16"
  enable_dns_support = true

 tags {
    EnvironmentName = "${var.environment_name}"
    Name            = "${join("-","${list(var.environment_name, "VPC")}")}"
  }
}

  resource "aws_elb" "ELB" {
  name                = "${var.ELB}"
  subnets  = [
                "${aws_subnet.pub_subnet_a.id}",
                "${aws_subnet.pub_subnet_b.id}",
             ]
  security_groups = [ "$aws_security_group.ELB_sg.id" ]

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 8
    timeout             = 10
    target              = "HTTP:8080/"
    interval            = 30
  }

  listener {
    instance_port     = 8080
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

 tags {
    EnvironmentName = "${var.environment_name}"
    Name            = "${join("-","${list(var.environment_name, "ELB")}")}"
  }
}

data "aws_availability_zones" "env_available_azs" {}

resource "aws_subnet" "pub_subnet_a" {
  vpc_id                  = "${aws_vpc.env_vpc.id}"
  cidr_block              = "172.31.1.0/24"
  availability_zone       = "${data.aws_availability_zones.env_available_azs.names[var.availability_zones[0]]}"
  map_public_ip_on_launch = true

  tags {
    EnvironmentName = "${var.environment_name}"
    Name            = "${join("-","${list(var.environment_name, "Pub_Subnet_A")}")}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_subnet" "pub_subnet_b" {
  vpc_id                  = "${aws_vpc.env_vpc.id}"
  cidr_block              = "172.31.2.0/24"
  availability_zone       = "${data.aws_availability_zones.env_available_azs.names[var.availability_zones[1]]}"
  map_public_ip_on_launch = true
  tags {
    EnvironmentName = "${var.environment_name}"
    Name            = "${join("-","${list(var.environment_name, "Pub_Subnet_B")}")}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_subnet" "priv_subnet_a" {
  vpc_id                  = "${aws_vpc.env_vpc.id}"
  cidr_block              = "172.31.3.0/24"
  availability_zone       = "${data.aws_availability_zones.env_available_azs.names[var.availability_zones[0]]}"
  tags {
    EnvironmentName = "${var.environment_name}"
    Name            = "${join("-","${list(var.environment_name, "Priv_Subnet_A")}")}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_subnet" "priv_subnet_b" {
  vpc_id                  = "${aws_vpc.env_vpc.id}"
  cidr_block              = "172.31.4.0/24"
  availability_zone       = "${data.aws_availability_zones.env_available_azs.names[var.availability_zones[1]]}"
  tags {
    EnvironmentName = "${var.environment_name}"
    Name            = "${join("-","${list(var.environment_name, "Priv_Subnet_B")}")}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_internet_gateway" "env_igw" {
  vpc_id = "${aws_vpc.env_vpc.id}"

  tags {
    EnvironmentName = "${var.environment_name}"
    Name            = "${join("-","${list(var.environment_name, "IGW")}")}"
  }
}

resource "aws_route_table" "main_route_table" {
  vpc_id = "${aws_vpc.env_vpc.id}"

  tags {
    EnvironmentName = "${var.environment_name}"
    Name            = "${join("-","${list(var.environment_name, "RouteTablePublic")}")}"
  }
}

resource "aws_route" "internet_access" {
  route_table_id         = "${aws_route_table.main_route_table.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.env_igw.id}"
}

resource "aws_route_table_association" "env_RTAPub_a" {
  subnet_id      = "${aws_subnet.pub_subnet_a.id}"
  route_table_id = "${aws_route_table.main_route_table.id}"
}
resource "aws_route_table_association" "env_RTAPub_b" {
  subnet_id      = "${aws_subnet.pub_subnet_b.id}"
  route_table_id = "${aws_route_table.main_route_table.id}"
}

resource "aws_eip" "nat_gw1_eip" {
  vpc      = true
  depends_on = ["aws_internet_gateway.env_igw"]
}

resource "aws_eip" "nat_gw2_eip" {
  vpc      = true
  depends_on = ["aws_internet_gateway.env_igw"]
}

resource "aws_nat_gateway" "nat_gw1" {
    allocation_id = "${aws_eip.nat_gw1_eip.id}"
    subnet_id = "${aws_subnet.pub_subnet_a.id}"
    depends_on = ["aws_internet_gateway.env_igw"]
}

resource "aws_nat_gateway" "nat_gw2" {
    allocation_id = "${aws_eip.nat_gw2_eip.id}"
    subnet_id = "${aws_subnet.pub_subnet_b.id}"
    depends_on = ["aws_internet_gateway.env_igw"]
}

resource "aws_route_table" "private_route_table_a" {
    vpc_id = "${aws_vpc.env_vpc.id}"

  tags {
    EnvironmentName = "${var.environment_name}"
    Name            = "${join("-","${list(var.environment_name, "RouteTablePrivate_A")}")}"
  }
}

resource "aws_route_table" "private_route_table_b" {
    vpc_id = "${aws_vpc.env_vpc.id}"

  tags {
    EnvironmentName = "${var.environment_name}"
    Name            = "${join("-","${list(var.environment_name, "RouteTablePrivate_B")}")}"
  }
}

resource "aws_route" "private_route_a" {
	route_table_id  = "${aws_route_table.private_route_table_a.id}"
	destination_cidr_block = "0.0.0.0/0"
	nat_gateway_id = "${aws_nat_gateway.nat_gw1.id}"
}

resource "aws_route" "private_route_b" {
	route_table_id  = "${aws_route_table.private_route_table_b.id}"
	destination_cidr_block = "0.0.0.0/0"
	nat_gateway_id = "${aws_nat_gateway.nat_gw2.id}"
}

resource "aws_route_table_association" "env_RTAPriv_a" {
  subnet_id      = "${aws_subnet.priv_subnet_a.id}"
  route_table_id = "${aws_route_table.private_route_table_a.id}"
}

resource "aws_route_table_association" "env_RTAPriv_b" {
  subnet_id      = "${aws_subnet.priv_subnet_b.id}"
  route_table_id = "${aws_route_table.private_route_table_b.id}"
}

resource "aws_default_network_acl" "env_network_acl" {
  default_network_acl_id = "${aws_vpc.env_vpc.default_network_acl_id}"

  egress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags {
    EnvironmentName = "${var.environment_name}"
    Name            = "${join("-","${list(var.environment_name, "ACL")}")}"
  }

  subnet_ids = [
    "${aws_subnet.pub_subnet_a.id}",
    "${aws_subnet.pub_subnet_b.id}",
    "${aws_subnet.priv_subnet_a.id}",
    "${aws_subnet.priv_subnet_b.id}",
    ]
}

resource "aws_vpc_dhcp_options" "env_vpc_dhcp_options" {
  domain_name         = "${var.environment_name}"
  domain_name_servers = ["AmazonProvidedDNS"]

  tags {
    EnvironmentName = "${var.environment_name}"
    Name            = "${join("-","${list(var.environment_name, "DHCPOptions")}")}"
  }
}

resource "aws_vpc_dhcp_options_association" "env_vpc_dhcp_association" {
  vpc_id          = "${aws_vpc.env_vpc.id}"
  dhcp_options_id = "${aws_vpc_dhcp_options.env_vpc_dhcp_options.id}"
}


resource "aws_vpc_endpoint" "env_s3_endpoint" {
  vpc_id       = "${aws_vpc.env_vpc.id}"
  service_name = "${join(".","${list("com.amazonaws", var.region, "s3")}")}"

  route_table_ids = [
    "${aws_route_table.main_route_table.id}",
    "${aws_route_table.private_route_table_a.id}",
    "${aws_route_table.private_route_table_b.id}",
  ]
}

