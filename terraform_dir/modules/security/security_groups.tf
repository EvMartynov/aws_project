variable "vpc_id" {}

variable "environment_name" {}

resource "aws_security_group" "ec2_instance_sg" {
  vpc_id = "${var.vpc_id}"

  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 8080
    protocol = "tcp"
    to_port = 8080
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${join("-","${list(var.environment_name, "ec2_SG")}")}"
    EnvironmentName = "${var.environment_name}"
  }
}

resource "aws_security_group" "ELB_sg" {
  vpc_id = "${var.vpc_id}"

  ingress {
    from_port = 80
    protocol = "tcp"
    to_port = 8080
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${join("-","${list(var.environment_name, "ELB_SG")}")}"
    EnvironmentName = "${var.environment_name}"
  }
}