output "ec2_instance_sg_id" {
  value = "${aws_security_group.ec2_instance_sg.id}"
}
output "ELB_sg_id" {
  value = "${aws_security_group.ELB_sg.id}"
}